---
draft: false
title: "لینک‌های مفید"
subtitle: "دسترسی به لینک‌های مفید شیرازلاگ"
description: "دسترسی به لینک‌های مفید شیرازلاگ"
keywords: ["شیرازلاگ", "گنو/لینوکس", "متن‌باز", "نرم‌افزار آزاد", "لینوکس"]
date: "2017-08-10T15:12:46+04:30"
readmore: true
---

##### گروه کاربران لینوکس شیراز

- [کانال تلگرام](https://t.me/shirazlug)
- [گروه تلگرام جهت پرسش و پاسخ](http://tiny.cc/shiraz)
- [اینستاگرام](https://www.instagram.com/ShirazLUG.ir/)
- [پروژه‌های شیرازلاگ](https://framagit.org/shirazlug)

---

##### گروه تلگرام سایر لاگ‌ها در ایران
- [تهران](http://tiny.cc/teh)
- [اصفهان](http://tiny.cc/esfahan)
- [اراک](http://tiny.cc/arak)
- [اهواز](http://tiny.cc/ahwaz)
- [زنجان](http://tiny.cc/zanjan)
- [قزوین](http://tiny.cc/qazvin)
- [قم](http://tiny.cc/Qom)
- [کرمانشاه](http://tiny.cc/kermanshah)
- [گیلان](http://tiny.cc/guilan)
- [مشهد](http://tiny.cc/mashad)
- [بجنورد](http://tiny.cc/Bojnord)
- [مازندران](http://tiny.cc/mazandaran)
- [گرگان](http://t.me/Gorganlug)
- [همدان](http://tiny.cc/hmdlug)
