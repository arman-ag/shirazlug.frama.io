---
draft: false
title: "تیم اجرایی"
subtitle: "معرفی راهبران و تیم اجرایی شیراز لاگ"
keywords: "شیراز لاگ,تیم اجرایی، رهبران ۹۸، گرافیک، توسعه دهنده سایت،رادیو لاگ، شبکه‌های اجتماعی"
description: "معرفی راهبران و تیم اجرایی شیراز لاگ"
date: "2017-08-10T15:12:46+04:30"
readmore: true
---
## راهبران ۹۸
[![امین خزاعی](/img/team/khozaei.svg)](/members/khozaei/)
[![مریم بهزادی](/img/team/behzadi.svg)](/members/behzadi/)
![مهدی مهاجر](/img/team/mohajer.svg)
![علی مولایی](/img/team/molaei.svg)
![سید رحیم فیروزی](/img/team/firouzi.svg)

---

## گرافیک
[![مریم بهزادی](/img/team/behzadi.svg)](/members/behzadi/)
![محسن نظام‌الملکی](/img/team/nezam.svg)
[![امین خزاعی](/img/team/khozaei.svg)](/members/khozaei/)

---

## توسعهٔ سایت
[![مریم بهزادی](/img/team/behzadi.svg)](/members/behzadi/)
[![امین خزاعی](/img/team/khozaei.svg)](/members/khozaei/)
[![محمد میرشائی](/img/team/mirshaei.svg)](/members/mirshaei/)
![محسن نظام‌الملکی](/img/team/nezam.svg)
![وجیهه نیکخواه](/img/team/nikkhah.svg)
![پویا برزگر](/img/team/barzegar.svg)
![زهره بیضاوی](/img/team/beyzavi.svg)
[![بابک رزمجو](/img/team/razmjoo.svg)](/members/razmjoo/)
[![شهرام شایگانی](/img/team/shaygani.svg)](/members/shaygani/)

---

## رادیولاگ
![مهدی مهاجر](/img/team/mohajer.svg)

---

## شبکه‌های اجتماعی
![وجیهه نیکخواه](/img/team/nikkhah.svg)
![محمد شعاعی](/img/team/shoaei.svg)
[![محمد میرشائی](/img/team/mirshaei.svg)](/members/mirshaei/)
[![مریم بهزادی](/img/team/behzadi.svg)](/members/behzadi/)

---

## برنامه‌ریزی و هماهنگی جلسات
[![مریم بهزادی](/img/team/behzadi.svg)](/members/behzadi/)
[![امین خزاعی](/img/team/khozaei.svg)](/members/khozaei/)
![محمد شعاعی](/img/team/shoaei.svg)

<!-- TODO We need a method to automatically populate these lists -->
